<?php

namespace App\Http\Controllers;
use DB;

use App\Order;
use App\Orderdetail;

use Illuminate\Http\Request;

class OrdersController extends Controller
{

    /**
     * fetch order details
     * @param $id
     * @return array
     */
    public function fetchOrderData($id)
    {
        // your logic goes here.
       
        $bill_amount =0;
        $related_products = $order_details = $customerJson = $ret = $productDetails = array();
        $products = DB::table('orders')
                    ->join('customers','customers.customerNumber','orders.customerNumber')
                    ->join('orderdetails','orderdetails.orderNumber','orders.orderNumber')
                    ->join('products','products.productCode','orderdetails.productCode')                    
                    ->where('orders.orderNumber',$id)->paginate(12);
                   
        if (count($products) > 0) {
                        foreach ($products as $product) {
                            $totalline = $product->quantityOrdered * $product->priceEach;
                            $bill_amount += $totalline;
                           $order_details[] = array(
                            'product'=> $product->productName,
                            'product_line'=> $product->productLine,
                            'unit_price'=> $product->priceEach,
                            'qty'=> $product->quantityOrdered,
                            'line_total'=> $totalline,
                        );
                        $customerJson = array(                            
                            'first_name'=>$product->contactFirstName,
                            'last_name'=>$product->contactLastName,
                            'phone' => $product->phone,
                            'country_code' => $product->country,
                         );
                           
                        }
                       
                        $productDetails = array(
                        'order_id'=>$product->orderNumber,
                        'order_date'=>$product->orderDate,
                        'status'=>$product->status,
                        'order_details'=>$order_details,                       
                        'bill_amount' => round($bill_amount,2),
                        'customer'=>$customerJson,
                        
                    );

                    $ret['productDetails'] = $productDetails;
                    $ret['status'] = "success";
                    $ret['statusCode'] = (string) 200;
                   
                    return $ret;   
                     
                    }
                    else{
                        $related_products = array();
                        $ret['status'] = "400";
                        $ret['statusCode'] = (string) 400;
                        $ret['message'] = "Product not found";
                        return $ret;
                      
                    }

                    
        
    }
}
